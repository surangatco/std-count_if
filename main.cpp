#include<iostream>
#include<map>
#include<string>
#include<algorithm>


int main()
{
	//preparing the map
	std::map<std::string, int> map;
	map.emplace("suranga", 1);
	map.emplace("pathmakumara", 2);
	map.emplace("amal", 3);
	map.emplace("perera", 4);

	//the lambda provided at the end will select only the entries with my name. i.e "suranga" or "pathmakumara"
	auto noOfMatches = std::count_if(map.begin(), map.end(), [](std::pair<std::string, int> entry){
			if (entry.first=="suranga" || entry.first=="pathmakumara")
				return true;});

	std::cout<<"No of matches : "<< noOfMatches << std::endl;


return 0;
}
